﻿using AutoMapper;

using ApplicationModels = StudentService.Application.Contracts.Models;

using DomainModels = StudentService.Domain.Contracts.Models;

namespace StudentService.Application.Mapping
{
    public class ApplicationMappingProfile : Profile
    {
        public ApplicationMappingProfile()
        {
            CreateMap<DomainModels.StudentInfoModel, ApplicationModels.StudentInfoViewModel>()
                .ReverseMap();

            CreateMap<DomainModels.StudentGradeTypesEnum, ApplicationModels.StudentGradeTypesEnum>()
                .ReverseMap();
        }
    }
}