﻿using AutoMapper;

using Moq;

using StudentService.Domain.Contracts.Models;
using StudentService.Domain.Mapping;
using StudentService.Domain.Services;
using StudentService.Persistence.Contracts.Entities;
using StudentService.Persistence.Contracts.Interfaces;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Xunit;

namespace StudentService.Tests.Domain
{
    public class StudentInfoServiceTests
    {
        protected readonly IMapper _mapper;

        public StudentInfoModel MockStudentModel = new StudentInfoModel
        {
            Id = 1,
            FirstName = "FirstName",
            LastName = "LastName",
            Patronymic = "Patronymic",
            DateOfBirth = DateTime.Now,
            AverageGrade = StudentGradeTypesEnum.Good
        };

        public StudentInfoEntity MockStudentEntity = new StudentInfoEntity
        {
            Id = 1,
            FirstName = "FirstName",
            LastName = "LastName",
            Patronymic = "Patronymic",
            DateOfBirth = DateTime.Now,
            AverageGrade = 4
        };

        public StudentInfoServiceTests()
        {
            var mapperConfiguration = new MapperConfiguration(cfg => cfg.AddMaps(typeof(DomainMappingMarkerType)));
            _mapper = new Mapper(mapperConfiguration);
        }

        [Fact]
        public async Task GetAsync_GettedIsEqual()
        {
            var repositoryMock = new Mock<IStudentInfoRepository>();

            repositoryMock
                .Setup(s => s.GetAsync(MockStudentEntity.Id))
                .ReturnsAsync(MockStudentEntity);

            var service = new StudentInfoService(_mapper, repositoryMock.Object);

            var result = await service.GetAsync(MockStudentEntity.Id);

            Assert.Equal(result.Id, MockStudentEntity.Id);
        }

        [Fact]
        public async Task GetCollectionAsync_FirstGettedIsEqual()
        {
            var repositoryMock = new Mock<IStudentInfoRepository>();

            repositoryMock
                .Setup(s => s.GetCollectionAsync())
                .ReturnsAsync(
                    new List<StudentInfoEntity>() { MockStudentEntity });

            var service = new StudentInfoService(_mapper, repositoryMock.Object);

            var result = await service.GetCollectionAsync();

            Assert.Equal(result.First().Id, MockStudentEntity.Id);
        }

        [Fact]
        public async Task CreateAsync_CreatedIsEqual()
        {
            var repositoryMock = new Mock<IStudentInfoRepository>();

            repositoryMock
                .Setup(s => s.CreateAsync(It.IsAny<StudentInfoEntity>()))
                .ReturnsAsync(MockStudentEntity.Id);

            var service = new StudentInfoService(_mapper, repositoryMock.Object);

            var result = await service.CreateAsync(MockStudentModel);

            Assert.Equal(result, MockStudentEntity.Id);
        }

        [Fact]
        public async Task UpdateAsync_NotThrown()
        {
            var repositoryMock = new Mock<IStudentInfoRepository>();

            repositoryMock
                .Setup(s => s.UpdateAsync(It.IsAny<StudentInfoEntity>()));

            var service = new StudentInfoService(_mapper, repositoryMock.Object);

            await service.UpdateAsync(MockStudentModel);
        }

        [Fact]
        public async Task DeleteAsync_NotThrown()
        {
            var repositoryMock = new Mock<IStudentInfoRepository>();

            repositoryMock
                .Setup(s => s.DeleteAsync(It.IsAny<int>()));

            var service = new StudentInfoService(_mapper, repositoryMock.Object);

            await service.DeleteAsync(MockStudentModel.Id);
        }
    }
}