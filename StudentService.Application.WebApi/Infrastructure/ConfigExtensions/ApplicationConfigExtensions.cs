﻿using Microsoft.Extensions.DependencyInjection;

using StudentService.Application.Handlers.Command;
using StudentService.Application.Handlers.Query;

namespace StudentService.Application.WebApi.Infrastructure.ConfigExtensions
{
    public static class ApplicationConfigExtensions
    {
        public static IServiceCollection AddHandlers(this IServiceCollection services)
        {
            services.AddScoped<StudentInfoQueryHandler>();

            services.AddScoped<StudentInfoCommandHandler>();

            return services;
        }
    }
}