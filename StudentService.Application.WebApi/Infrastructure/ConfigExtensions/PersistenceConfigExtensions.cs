﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using StudentService.Persistence.Contracts.Interfaces;
using StudentService.Persistence.Contracts.Options;
using StudentService.Persistence.Repositories;
using StudentService.Persistence.Repositories.Contexts;
using StudentService.Persistence.Services;

namespace StudentService.Application.WebApi.Infrastructure.ConfigExtensions
{
    public static class PersistenceConfigExtensions
    {
        public static IServiceCollection AddPersistenceServices(this IServiceCollection services, IConfiguration configuration)
        {
            var studentInfoRepositoryConnectionString = configuration.GetConnectionString("StudentInfoRepositoryConnectionString");
            services.AddDbContext<StudentInfoRepositoryContext>(
                options => options.UseNpgsql(studentInfoRepositoryConnectionString));

            if (bool.TryParse(configuration.GetSection(nameof(RepositoriesCacheOptions))["UseCache"], out var useCache) && useCache)
            {
                services.AddScoped<StudentInfoRepository>();
                services.AddScoped<IStudentInfoRepository, StudentInfoRepositoryCacheService>();
            }
            else
            {
                services.AddScoped<IStudentInfoRepository, StudentInfoRepository>();
            }
            
            return services;
        }
    }
}