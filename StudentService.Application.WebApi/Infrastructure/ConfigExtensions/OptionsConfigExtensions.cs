﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using StudentService.Persistence.Contracts.Options;

namespace StudentService.Application.WebApi.Infrastructure.ConfigExtensions
{
    public static class OptionsConfigExtensions
    {
        public static IServiceCollection AddOptions(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<RepositoriesCacheOptions>(configuration.GetSection(nameof(RepositoriesCacheOptions)));

            return services;
        }
    }
}