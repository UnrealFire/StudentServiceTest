﻿using Microsoft.Extensions.DependencyInjection;

using StudentService.Application.Mapping;
using StudentService.Domain.Mapping;

namespace StudentService.Application.WebApi.Infrastructure.ConfigExtensions
{
    public static class AutoMapperConfigExtensions
    {
        public static IServiceCollection AddAutoMapperWithProfiles(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(ApplicationMappingMarkerType), typeof(DomainMappingMarkerType));

            return services;
        }
    }
}