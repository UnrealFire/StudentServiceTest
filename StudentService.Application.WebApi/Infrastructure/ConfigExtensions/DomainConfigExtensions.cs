﻿using Microsoft.Extensions.DependencyInjection;

using StudentService.Domain.Contracts.Interfaces;
using StudentService.Domain.Services;

namespace StudentService.Application.WebApi.Infrastructure.ConfigExtensions
{
    public static class DomainConfigExtensions
    {
        public static IServiceCollection AddDomainServices(this IServiceCollection services)
        {
            services.AddScoped<IStudentInfoService, StudentInfoService>();

            return services;
        }
    }
}