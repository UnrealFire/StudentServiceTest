﻿using Microsoft.AspNetCore.Mvc;

using StudentService.Application.Contracts.Command;
using StudentService.Application.Handlers.Command;

using System;
using System.Threading.Tasks;

namespace StudentService.Application.WebApi.Controllers.Command
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/command/student")]
    public class StudentInfoCommandController : Controller
    {
        protected readonly StudentInfoCommandHandler _handler;

        public StudentInfoCommandController(StudentInfoCommandHandler handler)
        {
            _handler = handler ?? throw new ArgumentNullException(nameof(handler));
        }

        [HttpPost("create")]
        public async Task<CreateStudentCommand.Result> CreateStudentAsync([FromBody] CreateStudentCommand command)
        {
            return await _handler.HandleAsync(command);
        }

        [HttpPost("update")]
        public async Task<IActionResult> UpdateStudentAsync([FromBody] UpdateStudentCommand command)
        {
            await _handler.HandleAsync(command);
            return Ok();
        }

        [HttpPost("delete")]
        public async Task<IActionResult> DeleteStudentAsync([FromBody] DeleteStudentCommand command)
        {
            await _handler.HandleAsync(command);

            return Ok();
        }
    }
}