﻿using Microsoft.AspNetCore.Mvc;

using StudentService.Application.Contracts.Query;
using StudentService.Application.Handlers.Query;

using System;
using System.Threading.Tasks;

namespace StudentService.Application.WebApi.Controllers.Query
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/query/student")]
    public class StudentInfoQueryController : Controller
    {
        protected readonly StudentInfoQueryHandler _handler;

        public StudentInfoQueryController(StudentInfoQueryHandler handler)
        {
            _handler = handler ?? throw new ArgumentNullException(nameof(handler));
        }

        [HttpGet("{Id:int}")]
        public async Task<GetStudentQuery.Result> GetStudentAsync([FromRoute] GetStudentQuery query)
        {
            return await _handler.HandleAsync(query);
        }

        [HttpPost("list")]
        public async Task<GetStudentCollectionQuery.Result> GetStudentCollectionAsync([FromBody] GetStudentCollectionQuery query)
        {
            return await _handler.HandleAsync(query);
        }
    }
}