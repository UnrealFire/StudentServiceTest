﻿using AutoMapper;

using DomainModels = StudentService.Domain.Contracts.Models;

using Entities = StudentService.Persistence.Contracts.Entities;

namespace StudentService.Domain.Mapping
{
    public class DomainMappingProfile : Profile
    {
        public DomainMappingProfile()
        {
            CreateMap<Entities.StudentInfoEntity, DomainModels.StudentInfoModel>()
                .ReverseMap();
        }
    }
}