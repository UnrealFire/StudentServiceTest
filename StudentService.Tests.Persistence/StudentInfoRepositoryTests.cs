﻿using Microsoft.EntityFrameworkCore;

using StudentService.Persistence.Contracts.Entities;
using StudentService.Persistence.Repositories;
using StudentService.Persistence.Repositories.Contexts;

using System;
using System.Linq;
using System.Threading.Tasks;

using Xunit;

namespace StudentService.Tests.Persistence
{
    public class StudentInfoRepositoryTests
    {

        public StudentInfoRepositoryTests()
        {
            
        }

        [Fact]
        public async Task GetAsync_GettedIsEqual()
        {
            using (var contextData = new StudentSeedDataFixture())
            {
                var repository = new StudentInfoRepository(contextData.StudentContext);

                var result = await repository.GetAsync(contextData.ExistingTestEntityId);

                Assert.Equal(result.Id, contextData.ExistingTestEntityId);
            }
        }

        [Fact]
        public async Task GetCollectionAsync_FirstGettedIsEqual()
        {
            using (var contextData = new StudentSeedDataFixture())
            {
                var repository = new StudentInfoRepository(contextData.StudentContext);

                var result = await repository.GetCollectionAsync();

                Assert.Equal(result.First().Id, contextData.ExistingTestEntityId);
            }
        }

        [Fact]
        public async Task CreateAsync_CreatedNotThrown()
        {
            using (var contextData = new StudentSeedDataFixture())
            {
                var repository = new StudentInfoRepository(contextData.StudentContext);

                var result = await repository.CreateAsync(
                    new StudentInfoEntity
                    {
                        FirstName = "FirstNameCreate",
                        LastName = "LastNameCreate",
                        Patronymic = "PatronymicCreate",
                        DateOfBirth = DateTime.Now,
                        AverageGrade = 4
                    });
            }
        }

        [Fact]
        public async Task UpdateAsync_NotThrown()
        {
            using (var contextData = new StudentSeedDataFixture())
            {
                var repository = new StudentInfoRepository(contextData.StudentContext);

                await repository.UpdateAsync(
                    new StudentInfoEntity
                    {
                        Id = contextData.ExistingTestEntityId,
                        FirstName = "FirstNameUpdate",
                        LastName = "LastNameUpdate",
                        Patronymic = "PatronymicUpdate",
                        DateOfBirth = DateTime.Now,
                        AverageGrade = 4
                    });
            }
        }

        [Fact]
        public async Task DeleteAsync_NotThrown()
        {
            using (var contextData = new StudentSeedDataFixture())
            {
                var repository = new StudentInfoRepository(contextData.StudentContext);

                await repository.DeleteAsync(contextData.ExistingTestEntityId);
            }
        }
    }

    public class StudentSeedDataFixture : IDisposable
    {
        public StudentInfoRepositoryContext StudentContext { get; private set; }

        public int ExistingTestEntityId { get; } = 1;

        public StudentSeedDataFixture()
        {
            var options = new DbContextOptionsBuilder<StudentInfoRepositoryContext>()
                .UseInMemoryDatabase("StudentInfoDatabase")
                .Options;

            StudentContext = new StudentInfoRepositoryContext(options);

            StudentContext.Database.EnsureCreated();

            StudentContext.StudentsInfo.Add(
                new StudentInfoEntity {
                    Id = ExistingTestEntityId,
                    FirstName = "FirstName",
                    LastName = "LastName",
                    Patronymic = "Patronymic",
                    DateOfBirth = DateTime.Now,
                    AverageGrade = 4
                });

            StudentContext.SaveChanges();
        }

        public void Dispose()
        {
            StudentContext.Database.EnsureDeleted();
            StudentContext.Dispose();
        }
    }
}