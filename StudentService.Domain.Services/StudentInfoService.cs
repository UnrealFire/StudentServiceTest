﻿using AutoMapper;

using StudentService.Domain.Contracts.Interfaces;
using StudentService.Domain.Contracts.Models;
using StudentService.Persistence.Contracts.Entities;
using StudentService.Persistence.Contracts.Interfaces;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StudentService.Domain.Services
{
    public class StudentInfoService : IStudentInfoService
    {
        protected readonly IMapper _mapper;

        protected readonly IStudentInfoRepository _studentInfoRepository;

        public StudentInfoService(IMapper mapper, IStudentInfoRepository studentInfoRepository)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));

            _studentInfoRepository = studentInfoRepository ?? throw new ArgumentNullException(nameof(studentInfoRepository));
        }

        public async Task<StudentInfoModel> GetAsync(int id)
        {
            var entity = await _studentInfoRepository.GetAsync(id);

            return _mapper.Map<StudentInfoModel>(entity);
        }

        public async Task<IEnumerable<StudentInfoModel>> GetCollectionAsync()
        {
            var entities = await _studentInfoRepository.GetCollectionAsync();

            return _mapper.Map<IEnumerable<StudentInfoModel>>(entities);
        }

        public async Task<int> CreateAsync(StudentInfoModel model)
        {
            var entity = _mapper.Map<StudentInfoEntity>(model);
            
            return await _studentInfoRepository.CreateAsync(entity);
        }

        public async Task UpdateAsync(StudentInfoModel model)
        {
            var entity = _mapper.Map<StudentInfoEntity>(model);

            await _studentInfoRepository.UpdateAsync(entity);
            
            return;
        }

        public async Task DeleteAsync(int id)
        {
            await _studentInfoRepository.DeleteAsync(id);

            return;
        }
    }
}