﻿using Microsoft.EntityFrameworkCore;

using StudentService.Persistence.Contracts.Entities;

namespace StudentService.Persistence.Repositories.Contexts
{
    public class StudentInfoRepositoryContext : DbContext
    {
        public DbSet<StudentInfoEntity> StudentsInfo { get; set; }

        public StudentInfoRepositoryContext(DbContextOptions<StudentInfoRepositoryContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}