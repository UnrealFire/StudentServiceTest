﻿using Microsoft.EntityFrameworkCore;

using StudentService.Persistence.Contracts.Entities;
using StudentService.Persistence.Contracts.Interfaces;
using StudentService.Persistence.Repositories.Contexts;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StudentService.Persistence.Repositories
{
    public class StudentInfoRepository : IStudentInfoRepository
    {
        protected readonly StudentInfoRepositoryContext _context;
        
        public StudentInfoRepository(StudentInfoRepositoryContext context)
        {
            _context = context ?? throw new ArgumentException(nameof(context));
        }

        public async Task<StudentInfoEntity> GetAsync(int id)
        {
            return await _context.StudentsInfo.FirstOrDefaultAsync(s => s.Id == id);
        }

        public async Task<IEnumerable<StudentInfoEntity>> GetCollectionAsync()
        {
            return await _context.StudentsInfo.ToListAsync();
        }

        public async Task<int> CreateAsync(StudentInfoEntity entity)
        {
            var createdEntity = _context.StudentsInfo.Add(entity);
            await _context.SaveChangesAsync();

            return createdEntity.Entity.Id;
        }

        public async Task UpdateAsync(StudentInfoEntity entity)
        {
            var existingEntity = await _context.StudentsInfo.FirstOrDefaultAsync(s => s.Id == entity.Id);

            if (existingEntity != null)
            {
                existingEntity.FirstName = entity.FirstName;
                existingEntity.LastName = entity.LastName;
                existingEntity.Patronymic = entity.Patronymic;
                existingEntity.DateOfBirth = entity.DateOfBirth;
                existingEntity.AverageGrade = entity.AverageGrade;

                _context.StudentsInfo.Update(existingEntity);
                await _context.SaveChangesAsync();

                return;
            }
            else
            {
                throw new ArgumentException($"Student entity Id={entity.Id} does not exist");
            }
        }

        public async Task DeleteAsync(int id)
        {
            var entity = await _context.StudentsInfo.FirstOrDefaultAsync(s => s.Id == id);

            if (entity != null)
            {
                _context.StudentsInfo.Remove(entity);
                await _context.SaveChangesAsync();

                return;
            }
            else
            {
                throw new ArgumentException($"Student entity Id={id} does not exist");
            }  
        }
    }
}