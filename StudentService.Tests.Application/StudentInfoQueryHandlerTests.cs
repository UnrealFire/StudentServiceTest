﻿using AutoMapper;

using Moq;

using StudentService.Application.Contracts.Query;
using StudentService.Application.Handlers.Query;
using StudentService.Application.Mapping;
using StudentService.Domain.Contracts.Interfaces;
using StudentService.Domain.Contracts.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Xunit;

namespace StudentService.Tests.Application
{
    public class StudentInfoQueryHandlerTests
    {
        protected readonly IMapper _mapper;

        public StudentInfoQueryHandlerTests()
        {
            var mapperConfiguration = new MapperConfiguration(cfg => cfg.AddMaps(typeof(ApplicationMappingMarkerType)));
            _mapper = new Mapper(mapperConfiguration);
        }

        public StudentInfoModel MockStudent = new StudentInfoModel
        {
            Id = 1,
            FirstName = "FirstName",
            LastName = "LastName",
            Patronymic = "Patronymic",
            DateOfBirth = DateTime.Now,
            AverageGrade = StudentGradeTypesEnum.Good
        };

        [Fact]
        public async Task HandleAsyncGet_GettedIsEqual()
        {
            var serviceMock = new Mock<IStudentInfoService>();

            serviceMock
                .Setup(s => s.GetAsync(MockStudent.Id))
                .ReturnsAsync(MockStudent);

            var handler = new StudentInfoQueryHandler(_mapper, serviceMock.Object);

            var query = new GetStudentQuery
            {
                Id = MockStudent.Id
            };

            var result = await handler.HandleAsync(query);

            Assert.Equal(result.Student.Id, MockStudent.Id);
        }

        [Fact]
        public async Task HandleAsyncGetCollection_FirstGettedIsEqual()
        {
            var serviceMock = new Mock<IStudentInfoService>();

            serviceMock
                .Setup(s => s.GetCollectionAsync())
                .ReturnsAsync(
                    new List<StudentInfoModel>() { MockStudent });

            var handler = new StudentInfoQueryHandler(_mapper, serviceMock.Object);

            var query = new GetStudentCollectionQuery
            {
                Page = 1,
                PageSize = 5
            };

            var result = await handler.HandleAsync(query);

            Assert.Equal(result.PageResults.First().Id, MockStudent.Id);
        }
    }
}