﻿using AutoMapper;

using Moq;

using StudentService.Application.Contracts.Command;
using StudentService.Application.Handlers.Command;
using StudentService.Application.Mapping;
using StudentService.Domain.Contracts.Interfaces;
using StudentService.Domain.Contracts.Models;

using System;
using System.Threading.Tasks;

using Xunit;

namespace StudentService.Tests.Application
{
    public class StudentInfoCommandHandlerTests
    {
        protected readonly IMapper _mapper;

        public StudentInfoCommandHandlerTests()
        {
            var mapperConfiguration = new MapperConfiguration(cfg => cfg.AddMaps(typeof(ApplicationMappingMarkerType)));
            _mapper = new Mapper(mapperConfiguration);
        }

        public StudentInfoModel MockStudent = new StudentInfoModel
        {
            Id = 1,
            FirstName = "FirstName",
            LastName = "LastName",
            Patronymic = "Patronymic",
            DateOfBirth = DateTime.Now,
            AverageGrade = StudentGradeTypesEnum.Good
        };

        [Fact]
        public async Task HandleAsyncCreate_CreatedIsEqual()
        {
            var serviceMock = new Mock<IStudentInfoService>();

            serviceMock
                .Setup(s => s.CreateAsync(It.IsAny<StudentInfoModel>()))
                .ReturnsAsync(MockStudent.Id);

            var handler = new StudentInfoCommandHandler(_mapper, serviceMock.Object);

            var command = new CreateStudentCommand
            {
                FirstName = MockStudent.FirstName,
                LastName = MockStudent.LastName,
                Patronymic = MockStudent.Patronymic,
                DateOfBirth = MockStudent.DateOfBirth,
                AverageGrade = _mapper.Map<StudentService.Application.Contracts.Models.StudentGradeTypesEnum?>(MockStudent.AverageGrade)
            };

            var result = await handler.HandleAsync(command);

            Assert.Equal(result.CreatedStudentId, MockStudent.Id);
        }

        [Fact]
        public async Task HandleAsyncUpdate_NotThrown()
        {
            var serviceMock = new Mock<IStudentInfoService>();

            serviceMock
                .Setup(s => s.UpdateAsync(It.IsAny<StudentInfoModel>()));

            var handler = new StudentInfoCommandHandler(_mapper, serviceMock.Object);

            var command = new UpdateStudentCommand
            {
                Id = MockStudent.Id,
                FirstName = MockStudent.FirstName,
                LastName = MockStudent.LastName,
                Patronymic = MockStudent.Patronymic,
                DateOfBirth = MockStudent.DateOfBirth,
                AverageGrade = _mapper.Map<StudentService.Application.Contracts.Models.StudentGradeTypesEnum?>(MockStudent.AverageGrade)
            };

            await handler.HandleAsync(command);
        }

        [Fact]
        public async Task HandleAsyncDelete_NotThrown()
        {
            var serviceMock = new Mock<IStudentInfoService>();

            serviceMock
                .Setup(s => s.DeleteAsync(It.IsAny<int>()));

            var handler = new StudentInfoCommandHandler(_mapper, serviceMock.Object);

            var command = new DeleteStudentCommand
            {
                Id = MockStudent.Id
            };

            await handler.HandleAsync(command);
        }
    }
}