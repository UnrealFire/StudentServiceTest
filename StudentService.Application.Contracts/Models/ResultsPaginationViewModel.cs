﻿using System;

namespace StudentService.Application.Contracts.Models
{
    public class ResultsPaginationViewModel
    {
        public ResultsPaginationViewModel(int currentPage, int pageSize, int resultsTotal)
        {
            CurrentPage = currentPage;
            PageSize = pageSize;
            ResultsTotal = resultsTotal;
            
            PagesTotal = (int)Math.Ceiling(resultsTotal / (double)pageSize);
            HasPrevious = currentPage > 1;
            HasNext = currentPage < PagesTotal;
        }

        public int CurrentPage { get; set; }

        public int PageSize { get; set; }

        public int ResultsTotal { get; set; }

        public int PagesTotal { get; set; }

        public bool HasPrevious { get; set; }

        public bool HasNext { get; set; }
    }
}