﻿using System;

namespace StudentService.Application.Contracts.Models
{
    public class StudentInfoViewModel
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Patronymic { get; set; }

        public DateTime DateOfBirth { get; set; }

        public StudentGradeTypesEnum? AverageGrade { get; set; }
    }
}