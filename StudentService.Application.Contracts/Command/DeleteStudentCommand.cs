﻿using System;

namespace StudentService.Application.Contracts.Command
{
    public class DeleteStudentCommand
    {
        public int Id { get; set; }

        public void Validate()
        {
            if (Id <= 0)
                throw new ArgumentException("Invalid student Id");
        }
    }
}