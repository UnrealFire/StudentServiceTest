﻿using System;

namespace StudentService.Application.Contracts.Command
{
    public class UpdateStudentCommand : CreateStudentCommand
    {
        public int Id { get; set; }

        public override void Validate()
        {
            base.Validate();

            if (Id <= 0)
                throw new ArgumentException("Invalid student Id");
        }
    }
}