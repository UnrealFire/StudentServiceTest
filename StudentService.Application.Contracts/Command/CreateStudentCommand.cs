﻿using StudentService.Application.Contracts.Models;

using System;

namespace StudentService.Application.Contracts.Command
{
    public class CreateStudentCommand
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Patronymic { get; set; }

        public DateTime DateOfBirth { get; set; }

        public StudentGradeTypesEnum? AverageGrade { get; set; }

        public virtual void Validate()
        {
            if (string.IsNullOrWhiteSpace(FirstName))
                throw new ArgumentException("Invalid student First Name");
            if (string.IsNullOrWhiteSpace(LastName))
                throw new ArgumentException("Invalid student Last Name");
            if (string.IsNullOrWhiteSpace(Patronymic))
                throw new ArgumentException("Invalid student Patronymic");
            if (DateOfBirth == DateTime.MinValue || DateOfBirth > DateTime.Now)
                throw new ArgumentException("Invalid student Date of Birth");
        }

        public class Result
        {
            public int CreatedStudentId { get; set; }
        }
    }
}