﻿using StudentService.Application.Contracts.Models;

using System;
using System.Collections.Generic;

namespace StudentService.Application.Contracts.Query
{
    public class GetStudentCollectionQuery
    {
        public int Page { get; set; } = 1;

        public int PageSize { get; set; } = 20; 

        public void Validate()
        {
            if (Page <= 0)
                throw new ArgumentException("Invalid Page Number");

            if (Page <= 0)
                throw new ArgumentException("Invalid Page Size");
        }

        public class Result
        {
            public ResultsPaginationViewModel Pagination { get; set; }

            public IEnumerable<StudentInfoViewModel> PageResults { get; set; }
        }
    }
}