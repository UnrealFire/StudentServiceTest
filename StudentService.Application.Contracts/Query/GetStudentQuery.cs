﻿using StudentService.Application.Contracts.Models;

using System;

namespace StudentService.Application.Contracts.Query
{
    public class GetStudentQuery
    {
        public int Id { get; set; }

        public void Validate()
        {
            if (Id <= 0)
                throw new ArgumentException("Invalid student Id");
        }

        public class Result
        {
            public StudentInfoViewModel Student { get; set; }
        }
    }
}