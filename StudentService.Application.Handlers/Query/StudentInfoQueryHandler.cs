﻿using AutoMapper;

using StudentService.Application.Contracts.Models;
using StudentService.Application.Contracts.Query;
using StudentService.Domain.Contracts.Interfaces;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentService.Application.Handlers.Query
{
    public class StudentInfoQueryHandler
    {
        protected readonly IMapper _mapper;

        protected readonly IStudentInfoService _studentInfoService;

        public StudentInfoQueryHandler(IMapper mapper, IStudentInfoService studentInfoService)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));

            _studentInfoService = studentInfoService ?? throw new ArgumentNullException(nameof(studentInfoService));
        }

        public async Task<GetStudentQuery.Result> HandleAsync(GetStudentQuery query)
        {
            query.Validate();

            var studentDomainModel = await _studentInfoService.GetAsync(query.Id);

            return new GetStudentQuery.Result
            {
                Student = _mapper.Map<StudentInfoViewModel>(studentDomainModel)
            };
        }

        public async Task<GetStudentCollectionQuery.Result> HandleAsync(GetStudentCollectionQuery query)
        {
            query.Validate();

            var studentDomainModels = await _studentInfoService.GetCollectionAsync();
            var studentViewModels = _mapper.Map<IEnumerable<StudentInfoViewModel>>(studentDomainModels);

            var currentPage = query.Page;
            var pageSize = query.PageSize;
            var resultsTotal = studentViewModels.Count();

            var pageResults = studentViewModels
                .Skip((currentPage - 1) * pageSize)
                .Take(pageSize);

            return new GetStudentCollectionQuery.Result
            {
                PageResults = pageResults,
                Pagination = new ResultsPaginationViewModel(currentPage, pageSize, resultsTotal)
            };
        }
    }
}