﻿using AutoMapper;

using StudentService.Application.Contracts.Command;
using StudentService.Domain.Contracts.Interfaces;
using StudentService.Domain.Contracts.Models;

using System;
using System.Threading.Tasks;

namespace StudentService.Application.Handlers.Command
{
    public class StudentInfoCommandHandler
    {
        protected readonly IMapper _mapper;

        protected readonly IStudentInfoService _studentInfoService;

        public StudentInfoCommandHandler(IMapper mapper, IStudentInfoService studentInfoService)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));

            _studentInfoService = studentInfoService ?? throw new ArgumentNullException(nameof(studentInfoService));
        }

        public async Task<CreateStudentCommand.Result> HandleAsync(CreateStudentCommand command)
        {
            command.Validate();

            var createdStudentId = await _studentInfoService.CreateAsync(
                new StudentInfoModel
                {
                    FirstName = command.FirstName,
                    LastName = command.LastName,
                    Patronymic = command.Patronymic,
                    DateOfBirth = command.DateOfBirth,
                    AverageGrade = _mapper.Map<Domain.Contracts.Models.StudentGradeTypesEnum>(command.AverageGrade)
                });

            return new CreateStudentCommand.Result
            {
                CreatedStudentId = createdStudentId
            };
        }

        public async Task HandleAsync(UpdateStudentCommand command)
        {
            command.Validate();

            await _studentInfoService.UpdateAsync(
                new StudentInfoModel
                {
                    Id = command.Id,
                    FirstName = command.FirstName,
                    LastName = command.LastName,
                    Patronymic = command.Patronymic,
                    DateOfBirth = command.DateOfBirth,
                    AverageGrade = _mapper.Map<Domain.Contracts.Models.StudentGradeTypesEnum>(command.AverageGrade)
                });
        }

        public async Task HandleAsync(DeleteStudentCommand command)
        {
            command.Validate();

            await _studentInfoService.DeleteAsync(command.Id);

            return;
        }
    }
}