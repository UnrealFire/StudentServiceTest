﻿namespace StudentService.Domain.Contracts.Models
{
    public enum StudentGradeTypesEnum
    {
        Excellent = 5,
        Good = 4,
        Acceptable = 3,
        Bad = 2
    }
}