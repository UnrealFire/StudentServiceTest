﻿using StudentService.Domain.Contracts.Models;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace StudentService.Domain.Contracts.Interfaces
{
    public interface IStudentInfoService
    {
        public Task<StudentInfoModel> GetAsync(int id);

        public Task<IEnumerable<StudentInfoModel>> GetCollectionAsync();

        public Task<int> CreateAsync(StudentInfoModel model);

        public Task UpdateAsync(StudentInfoModel model);

        public Task DeleteAsync(int id);
    }
}