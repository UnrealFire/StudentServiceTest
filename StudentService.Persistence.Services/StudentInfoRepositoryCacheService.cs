﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;

using StudentService.Persistence.Contracts.Entities;
using StudentService.Persistence.Contracts.Interfaces;
using StudentService.Persistence.Contracts.Options;
using StudentService.Persistence.Repositories;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StudentService.Persistence.Services
{
    public class StudentInfoRepositoryCacheService : IStudentInfoRepository
    {
        protected readonly IMemoryCache _cache;
        protected readonly RepositoriesCacheOptions _cacheOptions;

        protected readonly StudentInfoRepository _repository;

        public StudentInfoRepositoryCacheService(IMemoryCache cache, IOptions<RepositoriesCacheOptions> cacheOptions, StudentInfoRepository repository)
        {
            _cache = cache ?? throw new ArgumentException(nameof(cache));
            _cacheOptions = cacheOptions?.Value ?? throw new ArgumentException(nameof(cacheOptions));

            _repository = repository ?? throw new ArgumentException(nameof(repository));
        }

        public async Task<StudentInfoEntity> GetAsync(int id)
        {
            return await _repository.GetAsync(id);
        }

        public async Task<IEnumerable<StudentInfoEntity>> GetCollectionAsync()
        {
            var key = $"{nameof(GetCollectionAsync)}";
            return await GetOrCreateAsync(key, r => r.GetCollectionAsync());
        }

        public async Task<int> CreateAsync(StudentInfoEntity entity)
        {
            return await _repository.CreateAsync(entity);
        }

        public async Task UpdateAsync(StudentInfoEntity entity)
        {
            await _repository.UpdateAsync(entity);
            return;
        }

        public async Task DeleteAsync(int id)
        {
            await _repository.DeleteAsync(id);
            return;
        }

        private async Task<T> GetOrCreateAsync<T>(string key, Func<IStudentInfoRepository, Task<T>> create, TimeSpan? expirationTimeout = null)
        {
            return await _cache.GetOrCreateAsync(key,
                    entry =>
                    {
                        entry.AbsoluteExpirationRelativeToNow = expirationTimeout ?? _cacheOptions.ExpirationTimeout;
                        return create(_repository);
                    });
        }
    }
}