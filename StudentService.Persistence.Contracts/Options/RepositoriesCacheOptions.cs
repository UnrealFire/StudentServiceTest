﻿using System;

namespace StudentService.Persistence.Contracts.Options
{
    public class RepositoriesCacheOptions
    {
        public bool UseCache { get; set; }

        public TimeSpan ExpirationTimeout { get; set; }
    }
}