﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StudentService.Persistence.Contracts.Entities
{
    public class StudentInfoEntity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Patronymic { get; set; }

        [Required]
        public DateTime DateOfBirth { get; set; }

        public int? AverageGrade { get; set; }
    }
}