﻿using StudentService.Persistence.Contracts.Entities;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace StudentService.Persistence.Contracts.Interfaces
{
    public interface IStudentInfoRepository
    {
        public Task<StudentInfoEntity> GetAsync(int id);

        public Task<IEnumerable<StudentInfoEntity>> GetCollectionAsync();

        public Task<int> CreateAsync(StudentInfoEntity entity);

        public Task UpdateAsync(StudentInfoEntity entity);

        public Task DeleteAsync(int id);
    }
}